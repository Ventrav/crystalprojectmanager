<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjetsReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_report');
            $table->unsignedBigInteger('id_projet');
            $table->unsignedBigInteger('charge');
        });
        Schema::table('projects_reports', function (Blueprint $table) {
            $table->foreign('id_report')
                ->references('id')->on('reports')
                ->onDelete("restrict")
                ->onUpdate("restrict");
            $table->foreign('id_projet')
                ->references('id')->on('projects')
                ->onDelete("restrict")
                ->onUpdate("restrict");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projets_reports');
    }
}
