<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getMyReports($id){
        return Report::with("projects")->where("id_user",$id)->get();
    }

    public function store(Request $req){
        $report = Report::create($req->report);
        foreach($req->report["projets"] as $projet){
            $report->projects()->attach($projet["id"], ["charge" => $projet["charge"]]);
        }

        $report = Report::with("projects")->find($report->id);
        return response()->json($report,201);

    }
}
