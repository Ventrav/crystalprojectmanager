<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }

    public function getAll(){
        return Project::all();
    }

    public function store(Request $req){
        $projet = Project::create($req->projet);
        return response()->json($projet,201);
    }
}
