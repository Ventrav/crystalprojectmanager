<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ["report_date","action","id_user"];

    public function projects(){
        return $this->belongsToMany(Project::class,'projects_reports','id_report','id_projet')->withPivot("charge");
    }
}
