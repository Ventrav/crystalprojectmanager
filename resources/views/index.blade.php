<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Crystal Project Manager</title>

        <!-- Fonts -->
        <style src="{{ mix("css/app.css") }}"></style>
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">


        <script src="{{ asset("js/app.js") }}" defer></script>

        <!-- Styles -->
    </head>
    <body>
        <div id="app">

        </div>
    </body>
</html>
