import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token: "",
        user: undefined
    },
    getters: {
        token: state => state.token,
        user: state => state.user
    },
    mutations: {
        SET_TOKEN(state, {token}){
            state.token = token;
        },
        SET_USER(state, {user}){
            state.user = user;
        }
    },
    actions: {
        set_token({commit},{token}){
            commit('SET_TOKEN',{token})
        },
        set_user({commit},{user}){
            commit('SET_USER',{user})
        }
    }
})