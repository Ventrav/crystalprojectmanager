import VueRouter from 'vue-router';
import Login from './components/login.vue';
import Acceuil from './components/acceuil.vue';
import Projets from './components/projets';
import CreateReport from './components/reports.vue';

export default new VueRouter({
    mode: 'history',
    routes: [{
        path: '/login',
        components: {
            default: Login
        },
        name: 'login'
    },
    {
        path: '/',
        components: {
            default: Acceuil
        },
        name: 'acceuil'
    },
    {
        path: '/projets',
        components: {
            default: Projets
        },
        name: 'projets'
    },
    {
        path: '/nouveau-compte-rendu',
        components: {
            default: CreateReport
        },
        name: 'create_report'
    },
    ]
});