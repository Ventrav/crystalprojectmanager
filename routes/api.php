<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login','Auth\LoginController@loginAPI');

Route::get('/projects','ProjectController@getAll');
Route::post('/projects','ProjectController@store');

Route::get('/users/{id}/reports','ReportsController@getMyReports');
Route::post('/reports','ReportsController@store');
